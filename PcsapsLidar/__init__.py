# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PcsapsLidar
                                 A QGIS plugin
 Point Cloud Simple Archaeological Process Script
                             -------------------
        begin                : 2016-11-08
        copyright            : (C) 2016 by Iggdrasil
        email                : contact@iggdrasil.net
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load PcsapsLidar class from file PcsapsLidar.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .pcsaps_lidar import PcsapsLidar
    return PcsapsLidar(iface)
